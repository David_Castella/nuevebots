﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogoAaju : MonoBehaviour
{
    public GameObject cambio;
    public GameObject act;

    // Start is called before the first frame update
    void Start()
    {
        GameObject madre = GameObject.Find("aaju"); //Busco el objeto madre
        ObjetoController codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
        TriggerDialogo activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
        if (codigo != null)
        {
            if (GameManger.Instance.Puzzles["TimeSkip"] == true)
            {
                GUI_Manager.Instance.ToggleDialogue();
                activacion.DialogoTrigger();
                act.SetActive(false);
                cambio.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}