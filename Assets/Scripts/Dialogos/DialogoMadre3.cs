﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogoMadre3 : MonoBehaviour
{
    public GameObject dialogo;

    // Start is called before the first frame update
    void Start()
    {
        GameObject madre = GameObject.Find("madre3"); //Busco el objeto madre
        ObjetoController codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
        TriggerDialogo activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
        if (codigo != null)
        {
            if (GameManger.Instance.Puzzles["Pesca"] == true && SceneManager.GetActiveScene().name == "Escena1" && GameManger.Instance.Puzzles["TimeSkip"] == false)
            {
                GUI_Manager.Instance.ToggleDialogue();
                activacion.DialogoTrigger();
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (dialogo.active == false && GameManger.Instance.Puzzles["Pesca"] == true && GameManger.Instance.Puzzles["TimeSkip"] == false)
        {
            GameManger.Instance.Puzzles["TimeSkip"] = true;
        }
    }
}
