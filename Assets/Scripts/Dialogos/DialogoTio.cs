﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogoTio : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject madre = GameObject.Find("tio"); //Busco el objeto madre
        ObjetoController codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
        TriggerDialogo activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
        if (codigo != null)
        {
            if (SceneManager.GetActiveScene().name == "EscenaPuerto" && GameManger.Instance.Puzzles["TimeSkip"] == false)
            {
                GUI_Manager.Instance.ToggleDialogue();
                activacion.DialogoTrigger();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
