﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogo
{
    // Esto es una clase publica para meter los dialogos
    public string name;

    [TextArea(3, 10)]
    public string[] sentences;
}
