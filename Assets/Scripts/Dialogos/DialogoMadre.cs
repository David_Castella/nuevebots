﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogoMadre : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject madre = GameObject.Find("madre"); //Busco el objeto madre
        ObjetoController codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
        TriggerDialogo activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
        if (codigo != null)
        {
            if (GameManger.Instance.Puzzles["Nieve"] == false && SceneManager.GetActiveScene().name == "Escena1" )
            {
                GUI_Manager.Instance.ToggleDialogue();
                activacion.DialogoTrigger();
            }

            if (GameManger.Instance.Puzzles["Pesca"] == false && SceneManager.GetActiveScene().name == "IgluExterior" && GameManger.Instance.Puzzles["DialogoTio"] == false)
            {
                GUI_Manager.Instance.ToggleDialogue();
                activacion.DialogoTrigger();
            }
        }
    }

}
