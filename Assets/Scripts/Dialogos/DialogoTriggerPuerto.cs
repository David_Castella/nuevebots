﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogoTriggerPuerto : MonoBehaviour
{
    private ObjetoController codigo;
    private TriggerDialogo activacion;
    // Start is called before the first frame update
    void Start()
    {
        GameObject madre = GameObject.Find("TriggerPuerto"); //Busco el objeto madre
        codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
        activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (codigo != null)
            {
                if (SceneManager.GetActiveScene().name == "IgluExterior" && GameManger.Instance.Puzzles["DialogoTio"] == true && GameManger.Instance.Puzzles["TimeSkip"] == false)
                {
                    GUI_Manager.Instance.ToggleDialogue();
                    activacion.DialogoTrigger();
                }
            }
        }
    }
}
