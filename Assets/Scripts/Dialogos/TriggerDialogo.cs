﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogo : MonoBehaviour
{
    public Dialogo dialogo;

    public void DialogoTrigger ()
    {
        FindObjectOfType<ManagerDialogo>().StartDialogo(dialogo); //Esto busca al objeto ManagerDialogo y activa la funcion StartDialogo
    }
}
