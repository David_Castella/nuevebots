﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogoMadre2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject madre = GameObject.Find("madre2"); //Busco el objeto madre
        ObjetoController codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
        TriggerDialogo activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
        if (codigo != null)
        {
            if (GameManger.Instance.Puzzles["DialogoTio"] == true && SceneManager.GetActiveScene().name == "Escena1" && GameManger.Instance.Puzzles["Pesca"] == false)
            {
                GUI_Manager.Instance.ToggleDialogue();
                activacion.DialogoTrigger();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
