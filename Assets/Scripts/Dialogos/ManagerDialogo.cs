﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerDialogo : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    public GameObject dialogBox;

    public Sprite[] images;

    public Image cabeza;

    private Queue<string> sentences;

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialogo(Dialogo dialogo)
    {

        nameText.text = dialogo.name; // Muestra en la UI el nombre del NPC

        sentences.Clear(); // Limpia la queue

        foreach (string sentence in dialogo.sentences)
        {
            sentences.Enqueue(sentence); //Mete las frases del dialogo en la queue
        }
        DisplayNextSentence(); //Empieza la funcion para mostrar las frases del dialogo
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0) //Si no quedan frases acaba la conversacion
        {
            GUI_Manager.Instance.ToggleDialogue();
            return;
        }
        string sentence = sentences.Dequeue(); // Si aun quedan frases en la cola las va sacando y mostrando en la UI
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            if (letter == '-' || letter == '=' || letter == '+')
            {
                if (letter == '-')
                {
                    cabeza.sprite = images[0];
                }
                if (letter == '=')
                {
                    cabeza.sprite = images[1];
                }
                if (letter == '+')
                {
                    cabeza.sprite = images[2];
                }
            }
            else
            {
                dialogueText.text += letter;
            }
            yield return null;
        }
    }

    //void EndDialogo()
    //{
    //    dialogBox.SetActive(false);

    //}
}
