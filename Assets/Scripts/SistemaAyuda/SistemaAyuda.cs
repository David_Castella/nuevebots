﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SistemaAyuda : MonoBehaviour
{
    public Text help_text;

    // Start is called before the first frame update
    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();

        string sceneName = currentScene.name;

        if (sceneName == "Escena1")
        {
            help_text.text = "Sal del iglu";
        }
        if (sceneName == "IgluExterior" && GameManger.Instance.Puzzles["DialogoTio"] == false)
        {
            help_text.text = "Ve al puerto (noreste)";
        }
        if (sceneName == "IgluExterior" && GameManger.Instance.Puzzles["DialogoTio"] == true)
        {
            help_text.text = "Ve a pescar (noroeste)";
        }
        if (sceneName == "IgluExterior" && GameManger.Instance.Puzzles["TimeSkip"] == true)
        {
            help_text.text = "Ve al puerto (noreste)";
        }
        if (sceneName == "EscenaAgujeros")
        {
            help_text.text = "Pesca la cena";
        }
        if (sceneName == "EscenaPuerto")
        {
            help_text.text = "Ve al barco";
        }
        if (sceneName == "EscenaBarco")
        {
            help_text.text = "Termina el trayecto";
        }
    }


    public void ActivarTexto()
    {
        GUI_Manager.Instance.ToggleHelp();
    }
}
