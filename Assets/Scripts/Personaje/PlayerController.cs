﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private static PlayerController instance;

    [SerializeField]
    private bool onIce = false;
    [SerializeField]
    private float speed;

    private Animator myAnimator;

    private bool enableMovement = true;

    private Vector2 movement;

    public GameObject e_btn;

    Rigidbody2D rigidbody2d;
    AudioSource audionuevo;
    Vector2 lookDirection = new Vector2(1, 0);

    public static PlayerController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerController>();
            }

            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();          //Inicializamos el RB con el componente que tiene el objeto asignado
        audionuevo = GetComponent<AudioSource>();           //Inicializamos el objeto de Audio que tiene el protagonista asgnado
        myAnimator = GetComponent<Animator>();              //Inicializamos el animator para gestionar las animaciones
    }

    // Update is called once per frame
    void Update()
    {
        ManageAnimations();

        GetMovementAxis();

        if (!Mathf.Approximately(movement.x, 0.0f) || !Mathf.Approximately(movement.y, 0.0f))
        {
            lookDirection.Set(movement.x, movement.y);
            lookDirection.Normalize();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 3.0f, LayerMask.GetMask("NPCS"));

            if (hit.collider != null)
            {
                TriggerDialogo activacion = hit.collider.GetComponent<TriggerDialogo>();

                GUI_Manager.Instance.ToggleDialogue();               //Esto activa la UI
                activacion.DialogoTrigger();                        //Esto empieza el dialogo             
            }
        }

        //Debug Tool
        if (Input.GetKeyDown(KeyCode.K))
        {
            GUI_Manager.Instance.ChestAddItem(GUI_Manager.Item.Pez);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            GUI_Manager.Instance.InventoryAddItem(GUI_Manager.Item.Pez);
        }


    }




    void FixedUpdate()
    {
        if (!onIce)
            rigidbody2d.MovePosition(rigidbody2d.position + movement * speed * Time.deltaTime); //movimiento modificando el valor transform directamente para un movimiento más preciso
        else
                rigidbody2d.AddForce(movement * speed);     //movimiento usando físicas simulando mov sobre hielo. 

        if (rigidbody2d.velocity.x > 4) rigidbody2d.velocity = new Vector3 (3,1,0);         
        if (rigidbody2d.velocity.y > 4) rigidbody2d.velocity = new Vector3 (1,3,0);         


        if (!isMoving())
            audionuevo.Stop();
        else
            if (!audionuevo.isPlaying)
                audionuevo.Play();
    }


    private void ManageAnimations()
    {
        if (movement.x < 0) transform.localEulerAngles = new Vector3(0, 180, 0);
        else transform.localEulerAngles = new Vector3(0, 0, 0);

        if (!isMoving()) myAnimator.SetBool("dig", false);
        
        myAnimator.SetBool("idle", !isMoving());
        myAnimator.SetFloat("vertical", movement.y);

        if (movement.x != 0) myAnimator.SetBool("horizontal", true);
        else myAnimator.SetBool("horizontal", false);

    }



    private void GetMovementAxis()
    {
        if (enableMovement)
        {
            movement.x = Input.GetAxisRaw("Horizontal");        //Pilla el valor de las letras A y D y de las flechas horizontales
            movement.y = Input.GetAxisRaw("Vertical");          //Pilla el valor de las letras W y S y de las flechas verticales
        }
        else
        {
            movement = new Vector2(0, 0);
        }
    }


    public bool isMoving() {  return movement != new Vector2(0, 0);}            //Devuelve true cuando el personaje se mueve  (velocidad es 0 en cualquier caso)

    public void EnableMovement(bool value)                                      //Función para controlar si el personaje se puede mover o no
    {
        enableMovement = value;
    }                                  

    public void PlayDigAnim()                                                   //función pública para poder acceder a la animación desde puzle
    {
        StartCoroutine(_PlayDigAnim());                                         //Ejecutamos la corutina  
    }

    IEnumerator _PlayDigAnim()                                                  //Corrutina para controlar los tiempos de la animación correctamente
    {
        myAnimator.SetBool("dig", true);                                        //Activamos la animación de cavar
        EnableMovement(false);                                                  //hacemos que no pueda andar mientras cava
        yield return new WaitForSeconds(0.444f);                                //Esperamos 0.444s (tiempo que dura la animación)
        myAnimator.SetBool("dig", false);                                       //Acabamos la animación
        EnableMovement(true);                                                   //El jugador ya puede andar

    }                                              

}
