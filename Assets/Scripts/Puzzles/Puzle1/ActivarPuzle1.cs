﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarPuzle1 : MonoBehaviour
{
    public GameObject ui_puzle1;
    public ManagerPuzle1 mg;

    private bool onTrigger = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && onTrigger && GameManger.Instance.Puzzles["Pesca"] == false && GameManger.Instance.Puzzles["DialogoTio"] == true)
        {
            ui_puzle1.SetActive(true);
            PlayerController.Instance.EnableMovement(false);
            GUI_Manager.Instance.SetAyudaEnabled(false);
        }
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (mg.contadorpeces < 3)
            {
                PlayerController.Instance.e_btn.SetActive(true);
                onTrigger = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (mg.contadorpeces < 3)
            {
                PlayerController.Instance.e_btn.SetActive(false);
                onTrigger = false;
            }

        }
    }
}
