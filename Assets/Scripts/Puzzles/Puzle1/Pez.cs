﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pez : MonoBehaviour
{
    public Transform[] destinos;
    public bool enDestino = false;
    private Rigidbody2D myRb;
    private Transform destino;
    public float speed = 5;
    // Start is called before the first frame update
    void Start()
    {

        myRb = GetComponent<Rigidbody2D>();
        destino = GetNewRandomNode();
    }

    // Update is called once per frame
    void Update()
    {


        Vector2 direction = (destino.position - transform.position);
        myRb.velocity = direction.normalized * speed;

        if(direction.x < 0)
        transform.localRotation = Quaternion.AngleAxis(90, new Vector3(0,0,1));
        else if(direction.x > 0)
        transform.localRotation = Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));


        if (Vector2.Distance(destino.position, transform.position) < 0.5) destino = GetNewRandomNode();
    }


    private Transform GetNewRandomNode()
    {
        int tmp;

        tmp = Random.Range(0, destinos.Length);

        return destinos[tmp];
    }
}
