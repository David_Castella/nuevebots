﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ManagerPuzle1 : MonoBehaviour
{
    public int contadorpeces;
    public int contadorclics;
    public GameObject molinillo;
    public GameObject ui_puzle1;
    public GameObject ui_peces;
    public GameObject[] peces;

    public Text contadorText;

    public Sprite[] molinilloFases;
    private Animator animatorMolinillo;

    public Texture2D lanza;
    public Texture2D raton;


    // Start is called before the first frame update
    void Start()
    {
        animatorMolinillo = molinillo.GetComponent<Animator>();
        animatorMolinillo.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
       contadorText.text = contadorpeces.ToString() + " / 3";
       molinillo.GetComponent<Image>().sprite = molinilloFases[contadorclics % molinilloFases.Length];

        if (contadorclics >= 10)
        {
            ui_peces.SetActive(true);
            molinillo.SetActive(false);
           if (ui_peces.active)
            {
                Cursor.SetCursor(lanza, Vector2.zero, CursorMode.ForceSoftware);
            }
        }
        
    }

    public void click()
    {
        animatorMolinillo.enabled = true;
        StartCoroutine(_ShakeAnim());
        contadorclics++;
    }

    public void PezCogido(int pez) // Se activa al coger un pez
    {
        contadorpeces++;
        peces[pez].SetActive(false); // Desactiva el pez clicado

        GUI_Manager.Instance.InventoryAddItem(GUI_Manager.Item.Pez);

        if (contadorpeces >= 3) //Si hay mas de 3 peces cierra el puzle
        {

            ui_puzle1.SetActive(false);
            ui_peces.SetActive(false);
            PlayerController.Instance.EnableMovement(true);
            GUI_Manager.Instance.SetAyudaEnabled(true);
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            GameManger.Instance.Puzzles["Pesca"] = true;
        }
    }

    IEnumerator _ShakeAnim()
    {
        animatorMolinillo.SetBool("play", true);
        yield return new WaitForSeconds(0.01f);
        animatorMolinillo.SetBool("play", false);
    }


}
