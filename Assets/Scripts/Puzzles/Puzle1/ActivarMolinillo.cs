﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarMolinillo : MonoBehaviour
{
    private float contador;
    public GameObject molinillo;
    public GameObject agujero;
    // Start is called before the first frame update
    void Start()
    {
        contador = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Bucle () //Se llama en cada click en el molinillo
    {
        if (contador > 8) // Si el jugador clica 8 veces se activan los peces
        {
            molinillo.SetActive(false);
            agujero.SetActive(true);
        }
        contador += 1;
    }
}
