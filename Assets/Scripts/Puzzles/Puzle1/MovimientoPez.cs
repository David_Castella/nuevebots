﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPez : MonoBehaviour
{
    public Transform origen;
    public float randx;
    public float randy;

    private RectTransform rectTransform;
    private Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        //InvokeRepeating("Movimiento", 0, 4); //Cada 4 segundos llama la funcion movimiento
        //InvokeRepeating("alCentro", 0, 1); //Cada 4 segundos llama la funcion movimiento
        direction = new Vector2();
        randx = origen.position.x;
        randy = origen.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 aPos = rectTransform.position;  //Movimiento
        aPos.x += randx * Time.deltaTime * 3;   //Movimiento
        aPos.y += randy * Time.deltaTime * 3;   //Movimiento

        direction = (aPos - new Vector2(transform.position.x, transform.position.y));

        if (direction.x < 0) rectTransform.localScale = new Vector3(0.5f,0.5f,1);
        else rectTransform.localScale = new Vector3(-0.5f, 0.5f, 1);


        rectTransform.position = Vector3.MoveTowards(transform.position, origen.position, Time.deltaTime* 60) ; //Movimiento
    }

    private void alCentro()
    {

    }

    private void Movimiento ()
    {
        randx = Random.Range(-25, 25); //Establece de forma aleatoria la velocidad del pez
        randy = Random.Range(-25, 25); //Establece de forma aleatoria la velocidad del pez
    }
}
