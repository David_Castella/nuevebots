﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Cinemachine.Utility;

public class Obstaculo : MonoBehaviour
{
    public float speed = 2.0f;
    private Rigidbody2D rb;
    private GameObject barco;


    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-speed, 0);
        barco = GameObject.Find("barco");



    }

    // Update is called once per frame
    void Update()
    {


        //if (transform.position.x < barco.transform.position.x*1.5) Destroy(this.gameObject);
        if (transform.position.x < -45) Destroy(this.gameObject);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Respawn"))  Destroy(this.gameObject);
    }
}
