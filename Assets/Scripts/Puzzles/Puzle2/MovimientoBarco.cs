﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MovimientoBarco : MonoBehaviour
{
    public bool puzle_activado;
    private Vector2 movement;

    Rigidbody2D rigidbody2d;

    // Start is called before the first frame update
    void Start()
    {
        puzle_activado = true;
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (puzle_activado)
        {
            movement.y = Input.GetAxisRaw("Vertical");
        }
    }

    void FixedUpdate()
    {
        rigidbody2d.velocity = new Vector2(3f, movement.y * 3);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Respawn")) SceneManager.LoadScene("EscenaBarco");

        if (collision.CompareTag("Finish")) SceneManager.LoadScene("Continuara");

    }
}
