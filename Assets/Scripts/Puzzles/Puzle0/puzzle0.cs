﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events; 

public class puzzle0 : MonoBehaviour
{
    //PERSONAJE
    private PlayerController personaje;

    //VARIABLES DE LA CAMARA
    public CinemachineVirtualCamera virtualCamera;
    private CinemachineBasicMultiChannelPerlin virtualCameraNoise;
    private float shakeTime = 0.7f;

    //FASES DEL PUZLE
    public Sprite[] stages;


    //VARIABLES DEL PUZZLE
    private SpriteRenderer mySprite;
    private Animator myAnimator;

    private int currentStage = -1;          
    public GameObject nieve;
    public GameObject pala; 
    private bool tienePala;
    AudioSource audiocavar;

    void Start()
    {
        mySprite = nieve.GetComponent<SpriteRenderer>();                                //variable sprite para cambiar el sprite segun la fase de puzle
        myAnimator = nieve.GetComponent<Animator>();                                    //variable animator para poder parar la animación cuando haga falta
        audiocavar = GetComponent<AudioSource>();                                       //Coger audio cavar
        personaje = GameObject.Find("Protagonista").GetComponent<PlayerController>();   //buscamos el player en la jerarquía para poder acceder a él
        pala.GetComponent<Interactuable>().enabled = true;
        if (virtualCamera) virtualCameraNoise = virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>(); //Asignamos la camara del cinemachine



    }

    // Update is called once per frame
    void Update()
    {

        CameraShake();

        if (nieve)      //Nieve debe de ser distinto de null
            if (nieve.GetComponent<Interactuable>().getTriggered() && tienePala)            //En caso de que el personaje interactue con la nieve y tenga la pala activa
            {
            myAnimator.gameObject.GetComponent<Animator>().enabled = false;                 //Desactivamos el controlador de animaciones una vez ya ha acabado
            IncreaseStage();                                                               //Incrementamos el estado del puzle
            PlayerController.Instance.PlayDigAnim();
            nieve.GetComponent<Interactuable>().setTriggered(false);                        //Acabamos la interacción poniendo el trigger a falso
            }

        if (pala)    //pala debe de ser distinto de null                                        
            if (pala.GetComponent<Interactuable>().getTriggered()) //Comprobamos si se realiza la interaccion con la pala
            {
                tienePala = true;


                if (pala)
                    GUI_Manager.Instance.InventoryAddItem(GUI_Manager.Item.Pala);
                Destroy(pala);
                nieve.GetComponent<Interactuable>().enabled = true;                        //Acabamos la interacción poniendo el trigger a falso
            }

        if (currentStage >= 0 && currentStage < 2)//Diferentes estados del sprite de la nieve cuando se retira con la pala
            mySprite.sprite = stages[currentStage];

        if (currentStage == 2) //Destruimos el GameObject una vez hemos retirado toda la nieve
        {
            currentStage = -1;
            Destroy(nieve);
            PlayerController.Instance.e_btn.SetActive(false);
            GameManger.Instance.Puzzles["Nieve"] = true;
        }
    }

    private void CameraShake()
    {
        //Valores de cinemachine para el camera shake
        if (virtualCamera != null || virtualCameraNoise != null)
        {
            if (shakeTime > 0)
            {
                virtualCameraNoise.m_AmplitudeGain = 1f;
                virtualCameraNoise.m_FrequencyGain = 3f;
                shakeTime -= Time.deltaTime;
            }
            else
            {
                virtualCameraNoise.m_AmplitudeGain = 0f;
                virtualCameraNoise.m_FrequencyGain = 0f;
            }
        }
    }


    
    public void IncreaseStage() { currentStage++; }
        
}
