﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPuzzle0 : MonoBehaviour
{
    [SerializeField]
    private GameObject nieve_interact;

    private void Start()
    {
        if (GameManger.Instance.Puzzles["Nieve"] == true) GameObject.Find("pala").SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        ///Se llama al puzle nº0 marcandolo como activo
        if (_collision.CompareTag("Player") && GameManger.Instance.Puzzles["Nieve"] == false)
        {
            PuzzleManager.Instance.InvokePuzzle(0);
        }
    }
}
