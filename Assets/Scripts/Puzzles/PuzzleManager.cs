﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    private static PuzzleManager _instance;
    private int currentPuzzle = -1; 

    public GameObject[] puzzles;

    //Se crea una instancia del PuzzleManager
    public static PuzzleManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PuzzleManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    


    //Se selecciona el puzle y se activa
    public void InvokePuzzle(int _puzle)
    {
        currentPuzzle = _puzle;
        puzzles[_puzle].SetActive(true);
    }

    
    public int GetCurrentPuzzle() { return currentPuzzle; }                     //Saber el puzle por el que vamos
    public void SetCurrentPuzzle(int _value ) { currentPuzzle = _value; }       // Cambiar el puzle que se va a ejecutar
}
