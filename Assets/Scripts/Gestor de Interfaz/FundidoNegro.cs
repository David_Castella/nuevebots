﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FundidoNegro : MonoBehaviour
{
    // the image you want to fade, assign in inspector
    public Image img;
    public GameObject fondo;

    void Start()
    {
        DesFundir();
    }

    public void DesFundir()
    {
        StartCoroutine(FadeImage(true));
        StartCoroutine(Desactivar());
    }

    public void Fundir()
    {
        fondo.SetActive(true);
        StartCoroutine(DarkenImage(true));
    }

    IEnumerator FadeImage(bool fadeAway)
    {
       
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }

    IEnumerator DarkenImage(bool fadeAway)
    {
        // loop over 1 second
        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            // set color with i as alpha
            img.color = new Color(1, 1, 1, i);
            yield return null;
        }
    }

    IEnumerator Desactivar()
    {
        yield return new WaitForSeconds(1.5f    );
        fondo.SetActive(false);
    }
}
