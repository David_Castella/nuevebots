﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GUI_Manager : MonoBehaviour
{

    public const int itemCant = 3;

    public enum Item
    {
        None,
        Pala,
        Pez
    };

    [SerializeField]



    #region GUI_Management
    public Sprite[] itemsSprites = new Sprite[itemCant];
    public ManagerDialogo managerDialogo;

    private GameObject dialogue;
    private GameObject inventory;
    private GameObject chest;
    private GameObject help_btn;
    private GameObject help;
    private Text help_text;

    private bool _dialogue;
    private bool _inventory;
    private bool _chest;
    private bool _help_btn;
    #endregion

    #region inventory_Management

    private Image[] items_Inventory = new Image[3];
    private Image[] items_chest_Inventory = new Image[3];
    private int[] myInventory = new int[3] { 0, 0, 0 };

    #endregion

    #region chest_Management
    private Image[] items_Chest = new Image[6];
    private int[] myChest = new int[3] { 0, 0, 0 };

    #endregion

    public Camera MainCamera;

    private static GUI_Manager instance;

    public static GUI_Manager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GUI_Manager>();
            }

            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //PILLAMOS LA INFORMACIÓN DEL GAMEMANAGER
        _dialogue = GameManger.Instance.dialogue_canvas;
        _inventory = GameManger.Instance.inventory_canvas;
        _chest = GameManger.Instance.chest_canvas;
        _help_btn = GameManger.Instance.help_btn_canvas;

        myInventory = GameManger.Instance.inventory;
        myChest = GameManger.Instance.chest;

        MainCamera = FindObjectOfType<Camera>();

        if (SceneManager.GetActiveScene().name != "Escena0") //Si la escena no es el menu cargamos los datos
        {
            dialogue = GameObject.Find("TextBox_Dialogue");
            inventory = GameObject.Find("Inventario");
            chest = GameObject.Find("Cofre_UI");
            help_btn = GameObject.Find("CajaAyuda");
            help = GameObject.Find("Ayuda");


            items_Inventory[0] = GameObject.Find("Objeto_inventario1").GetComponent<Image>();
            items_Inventory[1] = GameObject.Find("Objeto_inventario2").GetComponent<Image>();
            items_Inventory[2] = GameObject.Find("Objeto_inventario3").GetComponent<Image>();

            items_chest_Inventory[0] = GameObject.Find("Objeto_cofre_inventario1").GetComponent<Image>();
            items_chest_Inventory[1] = GameObject.Find("Objeto_cofre_inventario2").GetComponent<Image>();
            items_chest_Inventory[2] = GameObject.Find("Objeto_cofre_inventario3").GetComponent<Image>();

            items_Chest[0] = GameObject.Find("Cofre_Objeto1").GetComponent<Image>();
            items_Chest[1] = GameObject.Find("Cofre_Objeto2").GetComponent<Image>();
            items_Chest[2] = GameObject.Find("Cofre_Objeto3").GetComponent<Image>();
            items_Chest[3] = GameObject.Find("Cofre_Objeto4").GetComponent<Image>();
            items_Chest[4] = GameObject.Find("Cofre_Objeto5").GetComponent<Image>();
            items_Chest[5] = GameObject.Find("Cofre_Objeto6").GetComponent<Image>();

            managerDialogo = FindObjectOfType<ManagerDialogo>();
        }

        dialogue.SetActive(false);
        inventory.SetActive(false);
        chest.SetActive(false);
        help_btn.SetActive(false);


        /*if ( SceneManager.GetActiveScene().name == "IgluExterior") //Empieza la escena con el dialogo de la madre activado
        {
            dialogue.SetActive(true);
        }*/


    }

    public void ToggleDialogue()
    {
        _dialogue = GameManger.Instance.dialogue_canvas; 

        _dialogue = !_dialogue;
        dialogue.SetActive(_dialogue);

        PlayerController.Instance.EnableMovement(!_dialogue);

        GameManger.Instance.dialogue_canvas = _dialogue;
    }

    public void ToggleInventory()
    {
        _inventory = GameManger.Instance.inventory_canvas;

        UpdateInventory();

        _inventory = !_inventory;
        inventory.SetActive(_inventory);

        GameManger.Instance.inventory_canvas = _inventory;
    }

    public void ToggleChest()
    {
        _chest = GameManger.Instance.chest_canvas;
        UpdateChest();
        _chest = !_chest;
        chest.SetActive(_chest);

        PlayerController.Instance.EnableMovement(!_chest);

        GameManger.Instance.chest_canvas = _chest;
    }

    public void hideHelp()
    {
        
    }

    public void ToggleHelp()
    {
        _help_btn = GameManger.Instance.help_btn_canvas;
        _help_btn = !_help_btn;
        help_btn.SetActive(_help_btn);

        PlayerController.Instance.EnableMovement(!_help_btn);

        GameManger.Instance.help_btn_canvas = _help_btn;
    }
   
    public int InventoryAddItem(Item _item) //devuelve la posición en que se ha insertado el item en el inventario, SI EL INVENTARIO ESTÁ LLENO DEVUELVE -1
    {
        for(int i = 0; i < myInventory.Length; i++)
        {
            if (myInventory[i] == (int)Item.None)
            {
                myInventory[i] = (int)_item;
                GameManger.Instance.inventory[i] = (int)_item;
                items_Inventory[i].sprite = itemsSprites[(int)_item];
                items_Inventory[i].color = new Color(items_Inventory[i].color.r, items_Inventory[i].color.g, items_Inventory[i].color.b, 1f);
                return i;
            }
        }
        return -1;
    }


    public void InventoryRemoveItem(int i) //elimina el objeto en la posición que pasamos como argumento
    {
        if(i <= myInventory.Length)
        {
            GameManger.Instance.inventory[i] = 0;
            UpdateInventory();
        }

    }


    public int ChestAddItem(Item _item)     //devuelve la posición en que se ha insertado el item en el cofre, SI EL COFRE ESTÁ LLENO DEVUELVE -1
    {
        for (int i = 0; i < myChest.Length; i++)
        {
            if (myChest[i] == (int)Item.None) 
            {
                myChest[i] = (int)_item;                                                            //Añadimos el item al cofre
                GameManger.Instance.chest[i] = (int)_item;
                items_Chest[i].sprite = itemsSprites[(int)_item];                                   //Modificamos su sprite
                items_Chest[i].color = new Color(items_Chest[i].color.r, items_Chest[i].color.g, items_Chest[i].color.b, 1f); //Ponemos su Alfa a 1 para que se vea
                return i;
            }
        }
        return -1;
    }

    public void ChestRemoveItem(int i) //elimina el objeto en la posición que pasamos como argumento
    {
        if (i <= myChest.Length)
        {
            GameManger.Instance.chest[i] = 0;
            UpdateInventory();
        }

    }

    public void MoveInventaryToChest(int i)
    {
        myInventory = GameManger.Instance.inventory;
        if (myInventory[i] != 0 && !ChestIsFull())
        {
            ChestAddItem((Item)myInventory[i]);
            InventoryRemoveItem(i);
            UpdateChest();
        }

    }

    public void MoveChestToInventary(int i)
    {
        myChest = GameManger.Instance.chest;
        if (myChest[i] != 0 && !InventaryIsFull())
        {
            InventoryAddItem((Item)myChest[i]);
            ChestRemoveItem(i);
            UpdateChest();
        }
    }

    public bool InventaryIsFull()
    {
        return (myInventory[0] != 0) && (myInventory[1] != 0) && (myInventory[2] != 0);
    }

    public bool ChestIsFull()
    {
        bool full = true;
        for(int i = 0; i < myChest.Length; i++)
        {
            if (myChest[i] == 0) full = false;
        }
        return full;
    }


    public void UpdateInventory()
    {
        myInventory = GameManger.Instance.inventory;
    
        for(int i = 0; i < myInventory.Length; i++)
        {
            switch (myInventory[i])
            {
                case 0:
                    items_Inventory[i].sprite = itemsSprites[(int)Item.None];
                    items_Inventory[i].color = new Color(items_Inventory[i].color.r, items_Inventory[i].color.g, items_Inventory[i].color.b, 0f);
                    break;
                case 1:
                    items_Inventory[i].sprite = itemsSprites[(int)Item.Pala];
                    items_Inventory[i].color = new Color(items_Inventory[i].color.r, items_Inventory[i].color.g, items_Inventory[i].color.b, 1f);
                    break;
                case 2:
                    items_Inventory[i].sprite = itemsSprites[(int)Item.Pez];
                    items_Inventory[i].color = new Color(items_Inventory[i].color.r, items_Inventory[i].color.g, items_Inventory[i].color.b, 1f);
                    break;
                default:
                    break;
            }
        }

    }

    public void UpdateChest()
    {
        myChest = GameManger.Instance.chest;

        for (int i = 0; i < myChest.Length; i++)
        {
            switch (myChest[i])
            {
                case 0:
                    items_Chest[i].sprite = itemsSprites[(int)Item.None];
                    items_Chest[i].color = new Color(items_Chest[i].color.r, items_Chest[i].color.g, items_Chest[i].color.b, 0f);
                    break;
                case 1:
                    items_Chest[i].sprite = itemsSprites[(int)Item.Pala];
                    items_Chest[i].color = new Color(items_Chest[i].color.r, items_Chest[i].color.g, items_Chest[i].color.b, 1f);
                    break;
                case 2:
                    items_Chest[i].sprite = itemsSprites[(int)Item.Pez];
                    items_Chest[i].color = new Color(items_Chest[i].color.r, items_Chest[i].color.g, items_Chest[i].color.b, 1f);
                    break;
                default:
                    break;
            }
        }


        for (int i = 0; i < myInventory.Length; i++)
        {
            switch (myInventory[i])
            {
                case 0:
                    items_chest_Inventory[i].sprite = itemsSprites[(int)Item.None];
                    items_chest_Inventory[i].color = new Color(items_chest_Inventory[i].color.r, items_chest_Inventory[i].color.g, items_chest_Inventory[i].color.b, 0f);
                    break;
                case 1:;
                    items_chest_Inventory[i].sprite = itemsSprites[(int)Item.Pala];
                    items_chest_Inventory[i].color = new Color(items_chest_Inventory[i].color.r, items_chest_Inventory[i].color.g, items_chest_Inventory[i].color.b, 1f);
                    break;
                case 2:
                    items_chest_Inventory[i].sprite = itemsSprites[(int)Item.Pez];
                    items_chest_Inventory[i].color = new Color(items_chest_Inventory[i].color.r, items_chest_Inventory[i].color.g, items_chest_Inventory[i].color.b, 1f);
                    break;
                default:
                    break;
            }
        }
    }

    #region Getters & Setters

    public bool GetDialogue() { return _dialogue; }

    public void SetAyudaEnabled(bool value) { help.SetActive(value); }

    #endregion
}
