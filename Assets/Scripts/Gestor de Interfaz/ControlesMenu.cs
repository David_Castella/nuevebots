﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlesMenu : MonoBehaviour
{
    public GameObject controles;
    public bool abiertos;

    // Start is called before the first frame update
    void Start()
    {
        abiertos = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivarControles()
    {
        abiertos = !abiertos;
        controles.SetActive(abiertos);
    }
}
