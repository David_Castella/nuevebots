﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventarySystem : MonoBehaviour
{



    public List<Image> items;


    private Image[] inventory = new Image[3];

    public GameObject itemImages;
    public GameObject chest_window;

    private GameObject chest;




    public void Start()
    {

        //inventory[0] = GUI_Manager.Instance.items_Inventory[0];
        //inventory[1] = GUI_Manager.Instance.items_Inventory[1];
        //inventory[2] = GUI_Manager.Instance.items_Inventory[2];

        chest = GameObject.Find("cofre");
        //itemImages = GameObject.Find("ItemImages");
        //ShowAll();

    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))  GUI_Manager.Instance.ToggleInventory();    //Abrir o cerrar el inventario

        if ( Input.GetKeyDown(KeyCode.E))    
            if(chest)
                if(chest.GetComponent<Interactuable>().getInRange())
                    GUI_Manager.Instance.ToggleChest();     //Abrir o cerrar el cofre
    }

    //Añadimos objetos a la lista
    //public bool Inventory_AddItem(Sprite new_item)                       //devuelve true si ha podido añadir el item en el inventario;
    //{

    //    foreach (var item in inventory)
    //    {
    //        if (item)
    //        {
    //            item.sprite = new_item;
    //            item.color = new Color(item.color.r, item.color.g, item.color.b, 1f);
    //            return true;
    //        }
    //    }
    //    return false;
    //}


    public void UpdateInventory()
    {

    }


    //Oculta todas las imagenes
    public void HideAll()
    {
        foreach (var i in inventory) i.gameObject.SetActive(false);
    }
    //Oculta todas las imagenes
    public void ShowAll()
    {
        //foreach (var i in inventory) i.gameObject.SetActive(true);
    }

  
}
