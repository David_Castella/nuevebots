﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemImages : MonoBehaviour
{
    public Image[] items_images;
    public Image[] item_chest_inventory;
    public Image[] item_images_chest;

}
