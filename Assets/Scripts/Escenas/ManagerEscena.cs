﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerEscena : MonoBehaviour
{

    public GameObject fundido;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            FundidoNegro a = fundido.GetComponent<FundidoNegro>();
            a.Fundir();
            StartCoroutine(SalirEscena());
        }
    }

    IEnumerator SalirEscena()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("IgluExterior");
    }
}
