﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarSkip : MonoBehaviour
{
    public GameObject dialogo;
    public GameObject cambio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dialogo.activeInHierarchy == false && GameManger.Instance.Puzzles["TimeSkip"] == true)
        {
            cambio.SetActive(true);
        }
    }
}
