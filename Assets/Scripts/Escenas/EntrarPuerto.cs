﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EntrarPuerto : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactuable>().getTriggered() && GameManger.Instance.Puzzles["DialogoTio"] == false)
        {
            SceneManager.LoadScene("EscenaPuerto");
        }
        else if (GetComponent<Interactuable>().getTriggered() && GameManger.Instance.Puzzles["DialogoTio"] == true && GameManger.Instance.Puzzles["TimeSkip"] == true)
        {
            SceneManager.LoadScene("EscenaPuerto");
        }
    }
}
