﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSkipPuerto : MonoBehaviour
{
    public GameObject tio;
    public GameObject madre;

    // Start is called before the first frame update
    void Start()
    {
        if (GameManger.Instance.Puzzles["TimeSkip"] == true)
        {
            tio.SetActive(true);
            madre.SetActive(true);
            ObjetoController codigo = madre.GetComponent<ObjetoController>(); //Busco su codigo ObjetoController
            TriggerDialogo activacion = madre.GetComponent<TriggerDialogo>(); //Busco su codigo TriggerDialogo
            if (codigo != null)
            {
                if (GameManger.Instance.Puzzles["TimeSkip"] == true)
                {
                    GUI_Manager.Instance.ToggleDialogue();
                    activacion.DialogoTrigger();
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
