﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerSkip : MonoBehaviour
{
    public GameObject fundido;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            FundidoNegro a = fundido.GetComponent<FundidoNegro>();
            a.Fundir();
            StartCoroutine(SalirEscena());
            a.DesFundir();

            SceneManager.LoadScene("Escena1");
        }
    }

    IEnumerator SalirEscena()
    {
        yield return new WaitForSeconds(2);
        StopAllCoroutines();
    }
}
