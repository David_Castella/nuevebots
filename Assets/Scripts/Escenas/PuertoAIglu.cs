﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertoAIglu : MonoBehaviour
{
    public GameObject dialogo;
    public GameObject cambio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dialogo.active == false && GameManger.Instance.Puzzles["TimeSkip"] == false)
        {
            cambio.SetActive(true);
        }
    }
}
