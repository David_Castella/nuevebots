﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

public class Interactuable : MonoBehaviour
{
    #region PublicVars
    public bool popUp = true;
    public bool changeColor = true;
    #endregion

    #region PrivateVars
    private SpriteRenderer mySp;
    private GameObject character;
    private bool inRange = false;
    public float range = 3;

    private Vector3 currentScale;
    private Vector3 newScale;

    private bool triggered = false;

    #endregion


    private void Awake()
    {
        mySp = gameObject.GetComponent<SpriteRenderer>();
        character = PlayerController.Instance.gameObject;                                     //Inicializa Player buscandolo en la Jerarquía
        inRange = Vector3.Distance(transform.position, character.transform.position) < range; //Inicializa InRange
       
    }

    private void Start()
    {
        currentScale = transform.localScale;
        newScale = transform.localScale + new Vector3(0.1f, 0.1f, 1);
    }

    private void Update()
    {
        //Establece el valor inRange en cada frame
        inRange = Vector3.Distance(transform.position, character.transform.position) < range;

        //Comprueba si se ha intentado interactuar con el objeto
        if (inRange && Input.GetKeyDown(KeyCode.E))
        {
            triggered = true;
            Debug.Log("BotonE pulsado");
        }

        if (inRange)
        {
            PlayerController.Instance.e_btn.SetActive(true);
            Debug.Log("E activada");
        }

        UpdateValues();
        OnEnterFeedback();
    }

    private void UpdateValues()
    {
        //Reseteamos los valores si el jugador se aleja 
        if (!inRange)
        {
            triggered = false;
            PlayerController.Instance.e_btn.SetActive(false);
        }
    }


    private void OnEnterFeedback()
    {
        if (inRange)
        {
            if(changeColor && mySp) gameObject.GetComponent<SpriteRenderer>().color = Color.gray;

            if(popUp) transform.localScale = newScale;
        }

        else
        {
           if(mySp) mySp.color = Color.white;
            transform.localScale = currentScale;
        }



    }

    void OnDestroy()
    {
       // PlayerController.Instance.e_btn.SetActive(false);
    }
    

    #region Getters
    //GETTERS
    public bool getInRange() { return inRange; }
    public bool getTriggered() { return triggered;  }
    #endregion

    #region Setters
    //Setters
    public void setTriggered(bool tr) { triggered = tr; }
    #endregion

}
