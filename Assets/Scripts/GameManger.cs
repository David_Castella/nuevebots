﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManger : MonoBehaviour
{
    //Game data a guardar sobre la GUI
    #region GUI_vars                        

    public bool dialogue_canvas;
    public bool inventory_canvas;
    public bool chest_canvas;
    public bool help_btn_canvas;

    public int[] inventory = new int[3] { 0, 0, 0 };

    public int[] chest = new int[6] { 0, 0, 0, 0, 0, 0 };
    #endregion

    #region puzzles 

    public Dictionary<string, bool> Puzzles;

    #endregion

    public int targetFrameRate = 60;

    public static GameManger Instance;
    //Creamos una instancia de la clase que no se va a destruir, si al entrar en una escenam ya hay un GameManagerObject, el nuevo se va a eliminar para conservar el viejo con la informaicón almacenada correctamente

    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = targetFrameRate;
    }
    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;

            Puzzles = new Dictionary<string, bool>();

            Puzzles.Add("Nieve",false);
            Puzzles.Add("Pesca",false);
            Puzzles.Add("Pesca_cofre",false);
            Puzzles.Add("Barco",false);
            Puzzles.Add("DialogoTio", false);
            Puzzles.Add("TimeSkip", false);

        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }



    }


    public void LoadEscena1()
    {
        StartCoroutine(CargarEscena1());
    }

    public void SalirJuego()
    {
        Application.Quit();
    }

    IEnumerator CargarEscena1()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Escena1");
    }
}
