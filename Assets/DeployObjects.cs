﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployObjects : MonoBehaviour
{
    public GameObject[] obstacles;
    public float respawnTime = 1.0f;
    private GameObject barco;
    // Start is called before the first frame update
    void Start()
    {
        barco = GameObject.Find("barco");
        StartCoroutine(ObstacleWave());
    }

    private void SpawnObject()
    {
        int number = Random.Range(0, 2);
        GameObject aux = Instantiate(obstacles[number]) as GameObject;
        float posXSpawn = barco.transform.position.x + 40;
        float posYSpawn = barco.transform.position.y;
        Debug.Log(posXSpawn);
        aux.transform.position = new Vector2(posXSpawn, Random.Range((posYSpawn-2), (posYSpawn+3) ));
    }

    IEnumerator ObstacleWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            SpawnObject();
        }
    }

}
